<?php

namespace Drupal\Tests\crouton\Functional;

use Drupal\Core\Url;

/**
 * Provides functional test assertions for breadcrumbs.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
trait AssertBreadcrumbTrait {

  /**
   * Assert that certain breadcrumb links appear.
   *
   * The expected 'href' values will be processed prior to assertion.
   *
   * @param \Drupal\Core\Url|string|null $goto
   *   A page to load. The current page will be tested if NULL.
   * @param array $expected
   *   A sequence of associative arrays with an 'href' key whose value is the
   *   expected breadcrumb link path and a 'text' key whose value is the
   *   expected breadcrumb link text (not sanitized).
   *
   * @see ::processLink()
   *   For more information about the processing of 'href' values.
   */
  protected function assertBreadcrumbLinks($goto, array $expected) {
    if (isset($goto)) {
      $this->drupalGet($goto);
    }

    $expected = array_map([$this, 'processBreadcrumbLink'], $expected);

    $this->assertSame($expected, array_filter($expected), 'Expected breadcrumb links are valid');
    $this->assertSame($expected, $this->getBreadcrumbLinks());
  }

  /**
   * Fetch all breadcrumb links on the current page.
   *
   * @return array
   *   A sequence of associative arrays with an 'href' key whose value is the
   *   breadcrumb link path and a 'text' key whose value is the breadcrumb link
   *   text (not sanitized).
   */
  protected function getBreadcrumbLinks() {
    $results = [];

    foreach ($this->xpath('//nav[@aria-labelledby="system-breadcrumb"]//ol/li/a') as $element) {
      $results[] = [
        'href' => $element->getAttribute('href'),
        'text' => $element->getText(),
      ];
    }

    return $results;
  }

  /**
   * Process the supplied link for a breadcrumb assertion.
   *
   * The supplied 'href' value will be processed; an empty href will be replaced
   * with the front page URL, and any href not prefixed with a forward slash
   * will be prepended with the appropriate Drupal base prefix.
   *
   * @param array $link
   *   An associative array with an 'href' key whose value is the expected
   *   breadcrumb link path and a 'text' key whose value is the expected
   *   breadcrumb link text (not sanitized).
   *
   * @return array|null
   *   The input link with a processed value for the 'href' key on success, or
   *   NULL if the link is invalid.
   */
  protected function processBreadcrumbLink(array $link): ?array {
    $href = $link['href'] ?? FALSE;
    $text = $link['text'] ?? NULL;

    if (is_string($text) && is_string($href ?? '')) {
      if ($href == '') {
        $href = Url::fromRoute('<front>')->toString();
      }
      elseif (substr($href, 0, 1) !== '/') {
        $href = Url::fromUri('base:' . $href)->toString();
      }

      return [
        'href' => $href,
        'text' => $text,
      ];
    }

    return NULL;
  }

}
