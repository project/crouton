<?php

namespace Drupal\Tests\crouton\Functional;

use Drupal\menu_link_content\MenuLinkContentInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the menu-based breadcrumb builder.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @coversDefaultClass \Drupal\crouton\MenuBasedBreadcrumbBuilder
 * @group crouton
 */
class MenuBasedBreadcrumbBuilderFunctionalTest extends BrowserTestBase {

  use AssertBreadcrumbTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'crouton',
    'menu_link_content',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('system_breadcrumb_block');

    $crouton_settings = \Drupal::configFactory()->getEditable('crouton.settings');
    $crouton_settings->set('menu_name', 'main');
    $crouton_settings->save();
  }

  /**
   * Create a menu link to some arbitrary content.
   *
   * @param \Drupal\menu_link_content\MenuLinkContentInterface|null $parent
   *   An optional parent menu link.
   *
   * @return \Drupal\menu_link_content\MenuLinkContentInterface
   *   The resulting menu link.
   */
  protected function createMenuLink(?MenuLinkContentInterface $parent = NULL) {
    $entity_type_manager = \Drupal::entityTypeManager();

    $node_storage = $entity_type_manager->getStorage('node');
    /** @var \Drupal\node\NodeInterface */
    $node = $node_storage->create([
      'title' => $this->randomMachineName(),
      'type' => $this->drupalCreateContentType()->id(),
    ]);

    $node->save();

    $menu_link_content_storage = $entity_type_manager->getStorage('menu_link_content');
    /** @var \Drupal\menu_link_content\MenuLinkContentInterface */
    $link = $menu_link_content_storage->create([
      'title' => $node->label(),
      'parent' => isset($parent) ? $parent->getPluginId() : NULL,
      'provider' => 'menu_link_content',
      'menu_name' => 'main',
      'link' => [
        'uri' => "internal:/{$node->toUrl()->getInternalPath()}",
      ],
    ]);

    $link->save();

    return $link;
  }

  /**
   * Get the expected breadcrumb for the provided link.
   *
   * @param \Drupal\menu_link_content\MenuLinkContentInterface $link
   *   The link for which to get the expected breadcrumb.
   *
   * @return array
   *   An associative array with an 'href' key whose value is the menu link path
   *   and a 'text' key whose value is the menu link text (not sanitized).
   */
  protected function getExpectedBreadcrumbForLink(MenuLinkContentInterface $link): array {
    return [
      'href' => $link->getUrlObject()->getInternalPath(),
      'text' => $link->getTitle(),
    ];
  }

  /**
   * Data provider for ::testBreadcrumbs().
   */
  public function providerTestBreadcrumbs() {
    return [
      'no append current, no prepend front' => [
        FALSE,
        FALSE,
      ],
      'no append current, prepend front' => [
        FALSE,
        TRUE,
      ],
      'append current, no prepend front' => [
        TRUE,
        FALSE,
      ],
      'append current, prepend front' => [
        TRUE,
        TRUE,
      ],
    ];
  }

  /**
   * Test that menu-based breadcrumbs work as expected.
   *
   * @dataProvider providerTestBreadcrumbs
   */
  public function testBreadcrumbs(bool $append_current, bool $prepend_front) {
    $crouton_settings = \Drupal::configFactory()->getEditable('crouton.settings');
    $crouton_settings->set('append_current', $append_current);
    $crouton_settings->set('prepend_front', $prepend_front);
    $crouton_settings->save();

    $expected = [];

    if ($prepend_front) {
      $expected[] = [
        'href' => '',
        'text' => 'Home',
      ];
    }

    $expected[] = $this->getExpectedBreadcrumbForLink($link = $this->createMenuLink());
    $expected[] = $this->getExpectedBreadcrumbForLink($link = $this->createMenuLink($link));

    $link = $this->createMenuLink($link);
    if ($append_current) {
      $expected[] = $this->getExpectedBreadcrumbForLink($link);
    }

    $this->assertBreadcrumbLinks($link->getUrlObject(), $expected);
  }

}
