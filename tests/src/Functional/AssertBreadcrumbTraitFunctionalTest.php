<?php

namespace Drupal\Tests\crouton\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for AssertBreadcrumbTrait.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @coversDefaultClass \Drupal\Tests\crouton\Functional\AssertBreadcrumbTrait
 * @group crouton
 */
class AssertBreadcrumbTraitFunctionalTest extends BrowserTestBase {

  use AssertBreadcrumbTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
  ];

  /**
   * Data provider for ::testGetBreadcrumbLinks().
   */
  public function providerTestGetBreadcrumbLinks() {
    return [
      'performance configuration, without breadcrumb block' => [
        FALSE,
        [
          'access administration pages',
          'administer site configuration',
        ],
        'admin/config/development/performance',
        [],
      ],
      'performance configuration, with breadcrumb block' => [
        TRUE,
        [
          'access administration pages',
          'administer site configuration',
        ],
        'admin/config/development/performance',
        [
          [
            'href' => '',
            'text' => 'Home',
          ],
          [
            'href' => 'admin',
            'text' => 'Administration',
          ],
          [
            'href' => 'admin/config',
            'text' => 'Configuration',
          ],
          [
            'href' => 'admin/config/development',
            'text' => 'Development',
          ],
        ],
      ],
    ];
  }

  /**
   * Test breadcrumb retrieval for the current page.
   *
   * @dataProvider providerTestGetBreadcrumbLinks
   *
   * @covers ::getBreadcrumbLinks
   */
  public function testGetBreadcrumbLinks(bool $place_breadcrumb_block, array $user_permissions, string $page, array $expected) {
    if ($place_breadcrumb_block) {
      $this->drupalPlaceBlock('system_breadcrumb_block');
    }

    if (!empty($user_permissions)) {
      $this->drupalLogin($this->drupalCreateUser($user_permissions));
    }

    $this->assertBreadcrumbLinks($page, $expected);
  }

}
