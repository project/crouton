<?php

namespace Drupal\Tests\crouton\Unit;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\crouton\MenuBasedBreadcrumbBuilder;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Route;

/**
 * Unit tests for the menu-based breadcrumb builder.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @coversDefaultClass \Drupal\crouton\MenuBasedBreadcrumbBuilder
 * @group crouton
 */
class MenuBasedBreadcrumbBuilderTest extends UnitTestCase {

  /**
   * The admin context service.
   *
   * @var \Drupal\Core\Routing\AdminContext&\PHPUnit\Framework\MockObject\MockObject
   */
  protected $adminContext;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface&\PHPUnit\Framework\MockObject\MockObject
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminContext = $this->createMock(AdminContext::class);
    $this->adminContext
      ->method('isAdminRoute')
      ->willReturn(FALSE);

    $this->routeMatch = $this->createMock(RouteMatchInterface::class);
    $this->routeMatch
      ->method('getRouteObject')
      ->willReturn($this->createMock(Route::class));

    $cache_contexts_manager = $this->getMockBuilder(CacheContextsManager::class)
      ->disableOriginalConstructor()
      ->getMock();
    $cache_contexts_manager
      ->method('assertValidTokens')
      ->willReturn(TRUE);

    $container = new Container();
    $container->set('cache_contexts_manager', $cache_contexts_manager);
    \Drupal::setContainer($container);
  }

  /**
   * Get a breadcrumb builder for the supplied configuration and links.
   *
   * @param array $config
   *   A list of configuration values, keyed by their containing object.
   * @param array $links
   *   A list of active Drupal core Link object sequences, grouped by menu name.
   *
   * @return \Drupal\crouton\MenuBasedBreadcrumbBuilder
   *   The breadcrumb builder.
   */
  protected function getBuilderWithConfigAndMenuLinks(array $config = [], array $links = []): MenuBasedBreadcrumbBuilder {
    [$menu_active_trail, $menu_link_manager] = $this->getMenuServicesFromLinks($links);

    $config_factory = $this->getConfigFactoryStub($config);
    foreach (array_keys($config) as $name) {
      /** @var \Drupal\Core\Config\ImmutableConfig&\PHPUnit\Framework\MockObject\MockObject */
      $config = $config_factory->get($name);
      $config
        ->method('getCacheContexts')
        ->willReturn([]);
      $config
        ->method('getCacheMaxAge')
        ->willReturn(Cache::PERMANENT);
      $config
        ->method('getCacheTags')
        ->willReturn([]);
    }

    $builder = new MenuBasedBreadcrumbBuilder($this->adminContext, $config_factory, $menu_active_trail, $menu_link_manager);
    $builder->setStringTranslation($this->getStringTranslationStub());

    return $builder;
  }

  /**
   * Convert the supplied Drupal core Link and plugin ID into a menu link.
   *
   * @param \Drupal\Core\Link $link
   *   The link to convert to a menu link.
   * @param string $plugin_id
   *   The plugin ID to use for the resulting menu link.
   *
   * @return \Drupal\Core\Menu\MenuLinkInterface
   *   The resulting menu link.
   */
  protected function getMenuLinkFromCoreLinkAndPluginId(Link $link, string $plugin_id): MenuLinkInterface {
    $menu_link = $this->createMock(MenuLinkInterface::class);

    $menu_link
      ->method('getCacheContexts')
      ->willReturn([]);
    $menu_link
      ->method('getCacheMaxAge')
      ->willReturn(Cache::PERMANENT);
    $menu_link
      ->method('getCacheTags')
      ->willReturn([]);

    $menu_link
      ->method('getPluginId')
      ->willReturn($plugin_id);
    $menu_link
      ->method('getTitle')
      ->willReturn($link->getText());
    $menu_link
      ->method('getUrlObject')
      ->willReturn($link->getUrl());
    $menu_link
      ->method('isEnabled')
      ->willReturn(strpos($plugin_id, 'disabled:') !== 0);

    return $menu_link;
  }

  /**
   * Build the menu services used by the breadcrumb builder from a set of links.
   *
   * @param array $links
   *   An array of active link sequences, keyed by the corresponding menu name.
   *   Each menu's sequence should consist entirely of Drupal core Link objects.
   *
   * @return array
   *   An ordered list of menu services:
   *   - The menu active trail service.
   *   - The menu link manager service.
   */
  protected function getMenuServicesFromLinks(array $links): array {
    $menu_active_trail = $this->createMock(MenuActiveTrailInterface::class);
    $menu_link_manager = $this->createMock(MenuLinkManagerInterface::class);

    $get_active_link = [];
    $get_active_trail_ids = [];
    $create_instance = [];

    foreach ($links as $menu_name => $nested) {
      $active_trail_ids = array_reverse(array_combine(array_keys($nested), array_keys($nested))) + ['' => ''];
      $get_active_trail_ids[$menu_name] = [$menu_name, $active_trail_ids];

      if (!empty($nested)) {
        foreach ($nested as $plugin_id => $link) {
          $link = $this->getMenuLinkFromCoreLinkAndPluginId($link, $plugin_id);
          $create_instance[$plugin_id] = [$plugin_id, [], $link];
        }

        $get_active_link[$menu_name] = [$menu_name, $link ?? NULL];
      }
    }

    $menu_active_trail
      ->method('getActiveLink')
      ->willReturnMap($get_active_link);

    $menu_active_trail
      ->method('getActiveTrailIds')
      ->willReturnMap($get_active_trail_ids);

    $menu_link_manager
      ->method('createInstance')
      ->willReturnMap($create_instance);

    return [
      $menu_active_trail,
      $menu_link_manager,
    ];
  }

  /**
   * Data provider for ::testApplies().
   */
  public function providerTestApplies() {
    $inactive = $this->randomMachineName();
    $active = $this->randomMachineName();
    $disabled = $this->randomMachineName();
    $enabled = $this->randomMachineName();

    $links = [
      $inactive => [],
      $active => [
        'test1' => Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
        'test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
        'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
      ],
      $disabled => [
        'disabled:test1' => Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
        'test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
        'disabled:test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
      ],
      $enabled => [
        'disabled:test1' => Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
        'test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
        'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
      ],
    ];

    // This assertion ensures that we have distinct array keys.
    $this->assertEquals([
      $inactive,
      $active,
      $disabled,
      $enabled,
    ], array_keys($links));

    return [
      'has active link' => [
        [
          'crouton.settings' => [
            'menu_name' => $active,
          ],
        ],
        $links,
        TRUE,
      ],
      'has no active link' => [
        [
          'crouton.settings' => [
            'menu_name' => $inactive,
          ],
        ],
        $links,
        FALSE,
      ],
      'has disabled active link, disabled links not used' => [
        [
          'crouton.settings' => [
            'menu_name' => $disabled,
          ],
        ],
        $links,
        FALSE,
      ],
      'has disabled active link, disabled links used' => [
        [
          'crouton.settings' => [
            'menu_name' => $disabled,
            'use_disabled' => TRUE,
          ],
        ],
        $links,
        TRUE,
      ],
      'has enabled active link, disabled links not used' => [
        [
          'crouton.settings' => [
            'menu_name' => $enabled,
          ],
        ],
        $links,
        TRUE,
      ],
      'has enabled active link, disabled links used' => [
        [
          'crouton.settings' => [
            'menu_name' => $enabled,
            'use_disabled' => TRUE,
          ],
        ],
        $links,
        TRUE,
      ],
    ];
  }

  /**
   * Data provider for ::testAppliesAdminContext().
   */
  public function providerTestAppliesAdminContext() {
    $menu_name = $this->randomMachineName();

    $config = [
      'crouton.settings' => [
        'menu_name' => $menu_name,
      ],
    ];

    $links = [
      $menu_name => [
        'test1' => Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
        'test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
        'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
      ],
    ];

    return [
      'admin route' => [
        $config,
        $links,
        TRUE,
        FALSE,
      ],
      'not admin route' => [
        $config,
        $links,
        FALSE,
        TRUE,
      ],
    ];
  }

  /**
   * Data provider for ::testBuild().
   */
  public function providerTestBuild() {
    $menu_name = $this->randomMachineName();

    return [
      'append' => [
        [
          'crouton.settings' => [
            'append_current' => TRUE,
            'menu_name' => $menu_name,
          ],
        ],
        [
          $menu_name => [
            'test1' => Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
            'test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
            'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
          ],
        ],
        [
          Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
          Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
          Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')->setOption('attributes', ['aria-current' => 'page'])),
        ],
      ],
      'no append' => [
        [
          'crouton.settings' => [
            'menu_name' => $menu_name,
          ],
        ],
        [
          $menu_name => [
            'test1' => Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
            'test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
            'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
          ],
        ],
        [
          Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
          Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
        ],
      ],
      'no plain text' => [
        [
          'crouton.settings' => [
            'hide_plain_text' => TRUE,
            'menu_name' => $menu_name,
          ],
        ],
        [
          $menu_name => [
            'test1' => Link::fromTextAndUrl('Test 1', Url::fromRoute('<nolink>')),
            'test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
            'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
          ],
        ],
        [
          Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
        ],
      ],
      'use plain text' => [
        [
          'crouton.settings' => [
            'menu_name' => $menu_name,
          ],
        ],
        [
          $menu_name => [
            'test1' => Link::fromTextAndUrl('Test 1', Url::fromRoute('<nolink>')),
            'test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
            'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
          ],
        ],
        [
          Link::fromTextAndUrl('Test 1', Url::fromRoute('<nolink>')),
          Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
        ],
      ],
      'no disabled' => [
        [
          'crouton.settings' => [
            'menu_name' => $menu_name,
          ],
        ],
        [
          $menu_name => [
            'test1' => Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
            'disabled:test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
            'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
          ],
        ],
        [
          Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
        ],
      ],
      'use disabled' => [
        [
          'crouton.settings' => [
            'use_disabled' => TRUE,
            'menu_name' => $menu_name,
          ],
        ],
        [
          $menu_name => [
            'test1' => Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
            'disabled:test2' => Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
            'test3' => Link::fromTextAndUrl('Test 3', Url::fromUri('base:/test3')),
          ],
        ],
        [
          Link::fromTextAndUrl('Test 1', Url::fromUri('base:/test1')),
          Link::fromTextAndUrl('Test 2', Url::fromUri('base:/test2')),
        ],
      ],
    ];
  }

  /**
   * Test that the menu-based breadcrumb builder applies in the right scenarios.
   *
   * @dataProvider providerTestApplies
   *
   * @covers ::applies
   * @covers ::config
   * @covers ::getActiveLink
   */
  public function testApplies(array $config, array $links, bool $expected) {
    $builder = $this->getBuilderWithConfigAndMenuLinks($config, $links);
    $actual = $builder->applies($this->routeMatch);

    $this->assertEquals($expected, $actual);
  }

  /**
   * Test that admin routes are mutually exclusive with menu-based breadcrumbs.
   *
   * @dataProvider providerTestAppliesAdminContext
   *
   * @covers ::applies
   * @covers ::config
   * @covers ::getActiveLink
   */
  public function testAppliesAdminContext(array $config, array $links, bool $is_admin, bool $expected) {
    $this->adminContext = $this->createMock(AdminContext::class);
    $this->adminContext
      ->method('isAdminRoute')
      ->willReturn($is_admin);

    $builder = $this->getBuilderWithConfigAndMenuLinks($config, $links);
    $actual = $builder->applies($this->routeMatch);

    $this->assertEquals($expected, $actual);
  }

  /**
   * Tests that the breadcrumb builder produces the correct breadcrumb sequence.
   *
   * @dataProvider providerTestBuild
   *
   * @covers ::build
   * @covers ::config
   * @covers ::getActiveLink
   * @covers ::getActiveTrailLinks
   * @covers ::isActiveLink
   * @covers ::isMenuLinkApplicable
   * @covers ::processMenuLink
   */
  public function testBuild(array $config, array $links, array $expected) {
    $builder = $this->getBuilderWithConfigAndMenuLinks($config, $links);
    $actual = $builder->build($this->routeMatch)->getLinks();

    $this->assertEquals($expected, $actual);
  }

}
