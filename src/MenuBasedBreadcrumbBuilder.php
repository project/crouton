<?php

namespace Drupal\crouton;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Used to build breadcrumbs using the active menu item trail.
 *
 * Copyright (C) 2025  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class MenuBasedBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The admin context service.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The menu active trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * The menu link manager service.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * Constructs a MenuBasedBreadcrumbBuilder object.
   */
  public function __construct(AdminContext $admin_context, ConfigFactoryInterface $config_factory, MenuActiveTrailInterface $menu_active_trail, MenuLinkManagerInterface $menu_link_manager) {
    $this->adminContext = $admin_context;
    $this->configFactory = $config_factory;
    $this->menuActiveTrail = $menu_active_trail;
    $this->menuLinkManager = $menu_link_manager;
  }

  /**
   * Prepend a link to the front page (if desired).
   *
   * @param array $links
   *   The breadcrumb link sequence, passed by reference.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   Additional cacheable metadata accrued by this method.
   */
  protected function addFrontLink(array &$links): CacheableMetadata {
    $config = $this->config();

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($config);

    if (!empty($config->get('prepend_front'))) {
      array_unshift($links, Link::createFromRoute($this->t('Home'), '<front>'));
    }

    return $cacheability;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match, ?CacheableMetadata $cacheable_metadata = NULL) {
    $active = $this->getActiveLink();
    $config = $this->config();

    $cacheable_metadata?->addCacheableDependency($config);
    $cacheable_metadata?->addCacheContexts(['route']);

    $is_admin_route = $this->adminContext->isAdminRoute($route_match->getRouteObject());
    if (!$is_admin_route && isset($active)) {
      // The active menu trail does not consider whether a menu link is enabled.
      // Because of this, we're forced to fall back to the next applicable
      // breadcrumb builder when not allowed to use disabled links.
      return $active->isEnabled() || !empty($config->get('use_disabled'));
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $config = $this->config();
    $links = [];

    // The breadcrumb list will differ when the breadcrumb configuration is
    // updated, or when the active menu trail changes.
    $breadcrumb->addCacheableDependency($config);
    $breadcrumb->addCacheContexts(['route']);

    foreach ($this->getActiveTrailLinks() as $menu_link) {
      $breadcrumb->addCacheableDependency($menu_link);
      if ($link = $this->processMenuLink($menu_link)) {
        $links[] = $link;
      }
    }

    // Prepend a link to the front page (if desired).
    $breadcrumb->addCacheableDependency($this->addFrontLink($links));
    $breadcrumb->setLinks($links);

    return $breadcrumb;
  }

  /**
   * Get the configuration for this breadcrumb builder.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The configuration for this breadcrumb builder.
   */
  protected function config(): ImmutableConfig {
    return $this->configFactory->get('crouton.settings');
  }

  /**
   * Get the active menu link in the configured menu tree.
   *
   * This method will return NULL if a menu has not been selected in this
   * module's configuration for generating breadcrumbs.
   *
   * @return \Drupal\Core\Menu\MenuLinkInterface
   *   The active menu link in the configured menu tree.
   */
  protected function getActiveLink(): ?MenuLinkInterface {
    if ($menu_name = $this->config()->get('menu_name')) {
      return $this->menuActiveTrail->getActiveLink($menu_name);
    }

    return NULL;
  }

  /**
   * Get the active menu trail as a series of menu link plugins.
   *
   * This method will produce an empty sequence if a menu has not been selected
   * in this module's configuration for generating breadcrumbs.
   *
   * @return \Drupal\Core\Menu\MenuLinkInterface[]
   *   The active menu trail as a series of menu link plugins.
   */
  protected function getActiveTrailLinks(): array {
    $trail = [];

    if ($menu_name = $this->config()->get('menu_name')) {
      $trail = $this->menuActiveTrail->getActiveTrailIds($menu_name);
      $trail = array_reverse(array_filter($trail));
    }

    return array_map([$this->menuLinkManager, 'createInstance'], $trail);
  }

  /**
   * Check whether the supplied link is the active menu link.
   *
   * @param \Drupal\Core\Menu\MenuLinkInterface $menu_link
   *   The link which needs to be checked.
   *
   * @return bool
   *   TRUE if the supplied menu link is the active menu link, FALSE otherwise.
   */
  protected function isActiveLink(MenuLinkInterface $menu_link): bool {
    if ($active = $this->getActiveLink()) {
      return $active->getPluginId() === $menu_link->getPluginId();
    }

    return FALSE;
  }

  /**
   * Check if a menu link is applicable to this module's configured rules.
   *
   * @param \Drupal\Core\Menu\MenuLinkInterface $menu_link
   *   The link which needs to be checked for applicability.
   *
   * @return bool
   *   TRUE if the menu link is applicable, FALSE otherwise.
   */
  protected function isMenuLinkApplicable(MenuLinkInterface $menu_link): bool {
    $config = $this->config();

    // Check if this menu link is active and redundant.
    //
    // The active menu link should always be the last in the sequence, so it
    // should be safe to perform this check here instead of in aggregate scope.
    //
    // @see \Drupal\Core\Menu\MenuActiveTrail::doGetActiveTrailIds()
    // @see \Drupal\Core\Menu\MenuActiveTrail::getActiveLink()
    // @see \Drupal\Core\Menu\MenuTreeStorage::loadByRoute()
    if ($this->isActiveLink($menu_link) && empty($config->get('append_current'))) {
      return FALSE;
    }

    // Check if this menu link should be skipped due to being disabled.
    if (!$menu_link->isEnabled() && empty($config->get('use_disabled'))) {
      return FALSE;
    }

    $url = $menu_link->getUrlObject();

    // Check if this menu link consists solely of undesired plain-text.
    if ($url->isRouted() && $url->getRouteName() === '<nolink>' && !empty($config->get('hide_plain_text'))) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Process the supplied menu link to convert it to a plain link.
   *
   * This method may return NULL if the menu link is not applicable.
   *
   * @param \Drupal\Core\Menu\MenuLinkInterface $menu_link
   *   The link which needs to be processed.
   *
   * @return \Drupal\Core\Link|null
   *   A plain link produced from the supplied menu link, or NULL.
   */
  protected function processMenuLink(MenuLinkInterface $menu_link): ?Link {
    // Check if the menu link is not applicable.
    if (!$this->isMenuLinkApplicable($menu_link)) {
      return NULL;
    }

    $text = $menu_link->getTitle();
    $url = $menu_link->getUrlObject();

    // If the menu link is active, mark the resulting link as referring to the
    // current page for accessibility purposes.
    if ($this->isActiveLink($menu_link)) {
      $attributes = $url->getOption('attributes');
      $attributes['aria-current'] = 'page';

      $url->setOption('attributes', $attributes);
    }

    return Link::fromTextAndUrl($text, $url);
  }

}
