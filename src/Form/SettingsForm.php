<?php

namespace Drupal\crouton\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for this module.
 *
 * Copyright (C) 2024  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(current($this->getEditableConfigNames()));
    $form = parent::buildForm($form, $form_state);

    $form['info'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('This module introduces a breadcrumb builder that uses hierarchical menu information for pages with a menu link. Pages without a menu link will fall back to the next applicable breadcrumb builder and should be unaffected by this module.'),
    ];

    $form['menu_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Breadcrumb menu'),
      '#description' => $this->t('The menu to use when building breadcrumbs. If no menu is selected, breadcrumbs will be built by the next applicable breadcrumb builder (effectively disabling this module).'),
      '#default_value' => $config->get('menu_name'),
      '#empty_option' => $this->t('- None -'),
      '#options' => $this->getMenuOptions(),
    ];

    $form['prepend_front'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prepend a breadcrumb for the front page'),
      '#description' => $this->t('When enabled, a "Home" breadcrumb will be added to the front of the breadcrumb list. The name of this link can be changed with interface translation.'),
      '#default_value' => $config->get('prepend_front'),
    ];

    $form['append_current'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Append a breadcrumb for the current page'),
      '#description' => $this->t('When enabled, a breadcrumb for the current page will be included at the end of the breadcrumb list.'),
      '#default_value' => $config->get('append_current'),
    ];

    $form['use_disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use disabled menu items'),
      '#description' => $this->t('When enabled, the breadcrumb builder will produce breadcrumbs for ancestral menu items which are disabled. If both this setting and the active menu item are disabled, breadcrumbs will be built by the next applicable breadcrumb builder.'),
      '#default_value' => $config->get('use_disabled'),
    ];

    $form['hide_plain_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide plain-text breadcrumbs'),
      '#description' => $this->t('When enabled, the breadcrumb builder will ignore <code>&lt;nolink&gt;</code> menu links.'),
      '#default_value' => $config->get('hide_plain_text'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'crouton.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crouton_settings';
  }

  /**
   * Get a list of menu options.
   *
   * @return array
   *   A list of menu options.
   */
  protected function getMenuOptions(): array {
    $menus = $this->entityTypeManager->getStorage('menu')->loadMultiple();

    return array_map(function (EntityInterface $menu) {
      return $menu->label();
    }, $menus);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(current($this->getEditableConfigNames()));
    $config->setData($form_state->cleanValues()->getValues());
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
